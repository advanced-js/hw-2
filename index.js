"use strict";
/*
Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

Конструкція try... catch дозволяє нам впіймати помилку, якось її обробити та не дати зламатись всьому кодові. 
Передбачити все в if.. else неможливо. Наприклад, використовувати try catch доцільно, коли ми отримуємо якісь дані від користувача, вони можуть прийти не в тому форматі з різних причин, і замість того, щоб неправильний формат "убив" весь подальший код, ми можемо на цьому етапі спіймати помилку, якось її показати користувачеві та рухатися далі. Звичайно, якщо такі дані не критичні і без них можна рухатися далі. 
Приклад: є кнопка "завантажити файл", але завантажити файл не вдається, бо він видалений, в звичайному програма зупиняється. Але якщо обернути це в try catch, користувачеві виведеться месседж про те, що не вдалося завантажити файл і програма буде доступна для користувача, хоч і без цієї опції.

Інший приклад - ми відправили запрос на сервер, а сервер дав збій і прийшли невалідні дані. Тут також допоже try catch.

Також доречно використовувати цю конструкцію, коли ми пишем код та можемо припустити, що в цьому блоці может бути помилка.  

*/ 


const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

class Element{
    constructor(element, id, classes){
        this.element = element;
        this.id = id;
        this.classes = classes;
    }

    render(content){
        let element = document.createElement(this.element);   // створюю ul
        element.id = this.id;
        
        let requiredProperties = Object.values(["author", "name", "price"]); // визначаю, які властивості є обов'язковими
     
        let book = [];  // сюди попадатимуть ті книги, які відповідають умові

        //далі перебиратиму отриманий масив з книгами
        content.map((item) => {

          let instanceBook = `<li class=${this.classes}><b>«${item.name}»</b> (aвтор - <i>${item.author}</i>, ціна - <i>${item.price} грн</i>)</li>`;
    
          try{
                 let receivedProperty = Object.keys(item);  // визначаю, які властивості в кожній книжці


                 // перевіряю умову і додаю готову книгу або створюю помилку  
                requiredProperties.length === receivedProperty.length ? 

                book.push(instanceBook) :   

                requiredProperties.filter((n) => {                           
                    if(receivedProperty.indexOf(n) === -1){
                     let err = new Error(`Неможливо додати книгу «${item.name}» - немає ${getProperty(n)}`);
                     throw err;
                    } 
                     })

                  } 

          catch(err){
                console.error(`${err.name} - ${err.message}`);
                  }

});

       element.insertAdjacentHTML( "afterbegin", book.join("") );  // в ul додаю li
       return element;
       
} 
}


let ul = new Element( "ul", "books", "book-item");
const root = document.getElementById("root");
root.append( ul.render(books) ); // в div додаю готовий список



//роблю функцію, щоб виводила український переклад property
function getProperty(x){
    if(x === "price"){
       x = "ціни";
   } else if(x === "author"){
       x = "автора";
   }
     return x;
}


